websoak
=======

[![](https://drone.io/bitbucket.org/kludgy/websoak/status.png)](https://drone.io/bitbucket.org/kludgy/websoak/latest)

What is it?
-----------

A command line tool for simple HTTP request spamming. This implementation uses 
**forkIO** to generate a swarm of threads who open a connection and repeat the same
requests on that connection forever. Due to [GHC's](http://www.haskell.org/ghc/)
efficient implementation of lightweight threads, it is trivial to schedule 
thousands of concurrent threads on a modern desktop system.

Bug reports and suggestions are most welcomed. This is a bit of a learning 
experience for the author as well.


Why Would you Do This?
----------------------

There was need for such a tool to stress-test a proprietary server. [GHC](http://www.haskell.org/ghc/)
provided a means to do this simply and efficiently while retaining full 
programmability for future tests.


Usage
-----

By default, the GHC runtime system will run on a single OS thread:

    websoak -u"http://hostname:12345/path/to/resource?etc"

To use more OS threads, an additional pair of options are required:

    websoak -u"http://hostname:12345/path/to/resource?etc" +RTS -N

The **-N** above instructs the runtime system to utilize one OS thread per 
core. Excluding the port from a URL will cause the tool to use the
**--defaultport** value instead.

To continually replay a request log where each line is a URL, specify the file 
with:

    websoak --wlog=/path/to/file

The **name:port** portion of all URLs in a log file must match, and the tool 
will exit with an error if the rule is broken. The restriction exists because 
**websoak** operates 

Consult **websoak --help** for a summary of all options. In addition to the 
examples above, there are also some options for broadly controlling the 
frequency of requests and replays.

Use **CTRL+C** to halt execution of the tool.



Building
--------

1. Install the latest [Haskell Platform](http://www.haskell.org/platform/) for
   your system. Only [2013.2](http://www.haskell.org/platform/changelog.html) 
   has been verified to build so far.

2. Use cabal to build the tool: **cabal build** or **cabal install**.



Known Issues
------------

* Stream handles are not closed when the runtime exits. Because this 
  implementation can generate thousands of stream handles, the host OS may run
  out. These should time out eventually.

* The HTTP library will generate an error when the connection has failed:
  *websoak: Network/TCP.hs:358:57-76: Non-exhaustive patterns in record 
  update*. Errors leading to this issue can be observed by running with 
  **--verbose** output. 

* Lightweight thread generation on Windows is more limited. Too many at once, 
  and this error appears: *failed to create OS thread: Not enough storage is
  available to process this command.*
