{-# LANGUAGE DeriveDataTypeable, BangPatterns #-}

module Main (main, oneRequest) where

import Paths_websoak (version)
import Data.Version (showVersion)
import Prelude hiding (readFile) -- Because we want the strict version below
import Network.HTTP
import Network.URI
import Control.Monad
import Control.Concurrent.SwarmIO
import Control.Concurrent (threadDelay)
import System.Console.CmdArgs.Implicit
import System.IO.Strict (readFile)
import Safe (readDef)
import Data.Char

data WebSoak = WebSoak 
    { threads         :: Int
    , reportInterval  :: Int
    , url             :: String
    , wlog            :: String
    , requestDelay    :: Int
    , wlogDelay       :: Int
    , defaultPort     :: Int } deriving (Show, Data, Typeable)

webSoak = WebSoak
    { threads         = 500   &= help "Simultaneous connections to soak" 
    , reportInterval  = 500   &= help "Initialization report interval (per forked connections)" 
    , url             = ""    &= help "URL to soak if no wlog is specified"
    , wlog            = ""    &= help "Name of file with request URLs to soak"
    , requestDelay    = 100   &= help "Post-request delay (ms)"
    , wlogDelay       = 0     &= help "Post-wlog delay (ms)"
    , defaultPort     = 80    &= help "Default port"
    }
    &= verbosity
    &= help "Concurrent HTTP requests for scale testing."
    &= summary ("websoak-" ++ showVersion version)

main = do

    a <- cmdArgs webSoak

    when
        (threads a < 1)
        (error ("non-positive thread count: " ++ show (threads a)))

    l@(WLog _ requests) <- do
        content <- if null (wlog a) then return (url a) else readFile (wlog a)
        compile (Port $ defaultPort a) content

    when
        (null requests)
        (error "no requests in wlog")

    swarmIO 
        (normalProgressLogger (reportInterval a))
        (LongtermWorker (hammerRequests a l))
        (threads a)

    sleepForever

parseAuthPort :: Port -> String -> (AuthName,Port)
parseAuthPort (Port defPort) s =
    let
        uri = case parseURI s of
                        Nothing -> error ("malformed url: " ++ s)
                        Just x -> x
    
        auth = case uriAuthority uri of
                        Nothing -> error ("no authority: " ++ s)
                        Just x -> x

        -- The uriPort comes back as a string prefixed with the ':' character.
        -- The initial ':' is dropped and the number is deserialized with read.
        -- defPort is used if the given port string cannot be parsed.
        port = Port (readDef defPort . drop 1 $ uriPort auth)
    in
        (AuthName (uriRegName auth), port)

threadDelayMs :: Int -> IO ()
threadDelayMs t = threadDelay (t * 1000) -- ms => µs

hammerRequests :: WebSoak -> WLog -> IO ()
hammerRequests a (WLog (AuthName name, Port port) reqs) = do
    stream <- openTCPConnection name port
    let reqDelay = requestDelay a
    let logDelay = wlogDelay a
    forever $ do
        mapM 
            (\r -> do
                oneRequest stream r
                whenLoud (putStrLn $ "Waiting " ++ show reqDelay ++ "-ms before sending next request...")
                threadDelayMs reqDelay)
            reqs

        whenLoud (putStrLn $ "Waiting " ++ show logDelay ++ "-ms before restarting wlog...")
        threadDelayMs logDelay

oneRequest :: HandleStream [Char] -> Request_String -> IO ()
oneRequest stream req = do
    res <- simpleHTTP_ stream req
    whenLoud $ do
        putStrLn (">>>" ++ show req)
        case res of
            Left err -> putStrLn $ "ERROR: " ++ show err
            Right _ -> do 
                body <- getResponseBody res
                putStrLn $ "Response body: " ++ body

compile :: Port -> String -> IO WLog
compile defPort wlogContent = do
    let dom = map 
                (\l -> (parseAuthPort defPort l, getRequest l)) 
                (dropBlankLines . lines $ wlogContent)

    when 
        (null dom)
        (error "wlog empty (check --url or --wlog)")

    let ((name, port), _) = head dom
    let mismatchedAuthPort ((n,p),_) = name /= n || port /= p

    unless 
        (null (filter mismatchedAuthPort dom))
        (error "'name:port' must be the same for all requests in wlog")

    return $ WLog 
            (name,port) 
            (map (\(_,r) -> r) dom)

dropBlankLines :: [String] -> [String]
dropBlankLines ls = filter (not . null) . map (dropWhile isSpace) $ ls

normalProgressLogger :: Int -> ProgressHandler
normalProgressLogger every = ProgressHandler f where
    f prefix n i = whenNormal $ do
        when 
            (i == 1 || i == n || i `mod` every == 0)
            (putStrLn (prefix ++ " " ++ show i ++ "/" ++ show n))

data Port = Port !Int deriving (Show, Data, Typeable, Eq)
data AuthName = AuthName !String deriving (Show, Eq)
data WLog = WLog (AuthName,Port) ![Request_String] deriving (Show)
