module Control.Concurrent.SwarmIO 
	(	swarmIO
	,	sleepForever
	,	progressLogger
	,	ProgressHandler(..)
	,	LongtermWorker(..)				
	) where

import Control.Concurrent
import Control.Monad

-- | Fork a fixed number of long-term workers. The 'ProgressHandler' is applied
--   for each 'forkIO' call, which can provide useful feedback if the forking 
--   is perceptually delayed. 'swarmIO' returns immediately after forking the
--   given number of 'LongtermWorker's.
--
swarmIO :: ProgressHandler -> LongtermWorker -> Int -> IO ()
swarmIO (ProgressHandler p) (LongtermWorker s) n = do
    forM_ [1..n] (\i -> do p "Forked" n i ; forkIO s)

-- | Sugar for an effectively indefinite 'threadDelay'. Technically the thread
--   will awaken and sleep anew after each 'maxBound'-µs.
--
sleepForever :: IO ()
sleepForever = threadDelay maxBound >> sleepForever

-- | Example implementation of a 'ProgressHandler' that prints current/total
--   style progress at each given interval.
--
--   >>>swarmIO (progressLogger 500) f 10000
--   Forked 1/10000
--   Forked 500/10000
--   Forked 1000/10000
--   ...
--   Forked 10000/10000
--
progressLogger :: Int -> ProgressHandler
progressLogger every = ProgressHandler f where
	f prefix n i = 
		when (i == 1 || i == n || i `mod` every == 0) $ do
    		putStrLn (prefix ++ " " ++ show i ++ "/" ++ show n)

-- | Handle a progress update where the given 'prefixString' is for context.
--
--   >>>progress prefixString totalThreads thisThreadIndex
--
newtype ProgressHandler = ProgressHandler (String -> Int -> Int -> IO ())

newtype LongtermWorker = LongtermWorker (IO ())
